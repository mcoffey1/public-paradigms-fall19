import cherrypy
import json
from _movie_database import _movie_database

class ResetController(object):

	def __init__(self, mdb=None):
        	if mdb is None:
            		self.mdb = _movie_database()
        	else:
            		self.mdb = mdb

	def PUT_INDEX(self):
		output = {'result' : 'success'}
#		data = json.loads(cherrypy.request.body.read())

		try:
            		self.mdb.load_movies('/home/paradigms/ml-1m/movies.dat')
            		self.mdb.load_users('/home/paradigms/ml-1m/users.dat')
            		self.mdb.load_ratings('/home/paradigms/ml-1m/ratings.dat')
		except Exception as ex:
            		output['result'] = 'error'
            		output['message'] = str(ex)

		return json.dumps(output)


	def PUT_MID(self, movie_id):
		output = {'result' : 'success'}
		movie_id = int(movie_id)
        
		try:
            #		data = json.loads(cherrypy.request.body.read())
            		mdb_tmp = _movie_database()
            		mdb_tmp = _movie_database().load_movies('/home/paradigms/ml-1m/movies.dat')
            		items = mdb_tmp.get_movie(movie_id)
            		self.mdb.set_movie(mid, movie_id, items)
		except Exception as ex:
            		output['result'] = 'error'
            		output['message'] = str(ex)

		return json.dumps(output)
