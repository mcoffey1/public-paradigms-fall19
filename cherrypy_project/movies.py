import json
import cherrypy
from _movie_database import _movie_database


class MovieController(object):
    def __init__(self, mdb=None):
        if mdb is None:
             self.mdb = _movie_database()
        else:
             self.mdb = mdb
        self.posters = {}
        self.mdb.load_movies('ml-1m/movies.dat')
        self.load_posters('ml-1m/images.dat')


    def get_value(self, key):
        value = self.mdb[key]
        return value
	

    def get_poster_by_mid(self, mid):
        if mid in self.posters.keys():
            return self.posters[mid]
        return '/default.jpg'
    
    def load_posters(self, posters_file):
        self.posters = {}
        f = open(posters_file)
        for line in f:
            #TODO string parsing to get m_img
            split_line = line.split('::')
            mid = int(split_line[0])
            m_img = split_line[2].rstrip()
            self.posters[mid] = m_img


    # event handlers
    def GET_KEY(self, movie_id):
        # rule 1: set up some default output
        output = {'result': 'success'}

        # rule 2: check your data and type - key and payload
        key = int(movie_id)

        # rule 3: try - except blocks
        try:
            value = self.mdb.get_movie(key)
            if value is not None:
                output['id']        = int(movie_id)
                output['title']     = value[0]
                output['genres']    = value[1]
                output['img']       = self.get_poster_by_mid(key)
            else:
                output['result'] = 'error'
                output['message'] = 'movie not found'
        except KeyError as ex:
            output['result'] = 'error'
            output['message'] = 'key not found'
        except Exception as ex:
            output['result'] = 'error'
            output['message'] = str(ex)

        return json.dumps(output)

    def PUT_KEY(self, movie_id):
        output = {'result':'success'}
        key = int(movie_id)
        #extract msg from body
        data = cherrypy.request.body.read()
        data = json.loads(data)

        try:
            genre = data['genres']
            title = data['title']
            self.mdb.set_movie(key, [title, genre])
        except Exception as ex:
            output['result'] = 'error'
            output['message'] = str(ex)

        return json.dumps(output)

    def DELETE_KEY(self, movie_id):
        output = {'result':'success'}
        key = int(movie_id)
        try:
            self.mdb.delete_movie(key)
        except KeyError as ex:
            output['result'] = 'error'
            output['message'] = 'key not found'
        except Exception as ex:
            output['result'] = 'error'
            output['message'] = str(ex)
        return json.dumps(output)

    def DELETE_DICT(self):
        output = {'result':'success'}
        try:
            self.mdb.delete_movies()
        except Exception as ex:
            output['result'] = 'error'
            output['message'] = str(ex)
        return json.dumps(output)

    def GET_DICT(self):
        # rule 1: set up some default output
        output = {'result': 'success'}
        tmp_dict = {}
        tmp_list = []
        # rule 2: check your data and type - key and payload

        # rule 3: try - except blocks
        try:
            for movie in self.mdb.movies:
                tmp_dict = movie
                #tmp_dict['id'] = movie['mid']
                #tmp_dict['title'] = movie['mname']
                #tmp_dict['genres'] = movie['mgenres']
                tmp_dict.update({'img': self.get_poster_by_mid(movie['id'])})
                tmp_list.append(tmp_dict)
            output.update({'movies' : tmp_list})
        except KeyError as ex:
            output['result'] = 'error'
            output['message'] = 'key not found'
        except Exception as ex:
            output['result'] = 'error'
            output['message'] = str(ex)

        return json.dumps(output)

    def POST_DICT (self):
        output = {'result':'success'}
        body = cherrypy.request.body.read()
        body = json.loads(body)
        try:
            body.update({'id':(self.mdb.movies[-1]["id"])+1})
            self.mdb.movies.append(body)
            output.update({'id':(self.mdb.movies[-1]["id"])})
        except Exception as ex:
            output['result'] = 'error'
            output['message'] = str(ex)

        return json.dumps(output)


