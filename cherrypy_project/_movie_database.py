#!/usr/bin/env python3


class _movie_database:

       def __init__(self):
         self.movies = list()
         self.users = list()
         self.ratings = dict()

       def load_movies(self, movie_file):
        f = open(movie_file)
        self.movies = list()
        for line in f:
                line = line.rstrip()
                components = line.split("::")
                mid = int(components[0])
                mname = components[1]
                mgenres = components[2]

                self.movies.append({"id" : mid, "title" : mname, "genres" : mgenres})
        f.close()

       def load_users(self, user_file):
        self.users = list()
        f = open(user_file)
        for line in f:
                line = line.rstrip()
                components = line.split("::")
                uid = int(components[0])
                ugender = components[1]
                uage = int(components[2])
                uoccupation = int(components[3])
                uzip = components[4]

                self.users.append({"id" : uid, "gender" : ugender, "age" : uage, "occupation" : uoccupation, "zip" : uzip})
        f.close()

       def print_sorted_movies(self):
        self.movies = sorted(self.movies)
        for movie in self.movies:
          print(movie)
	
       def get_movie(self, index):
        ret = list()
        for i in self.movies:
         if (i["id"] == index):
          ret.append(i["title"])
          ret.append(i["genres"])
          return ret
        return None

       def get_movies(self):
        ret = list()
        for i in self.movies:
         ret.append(i["id"])
        return ret

       def set_movie(self, mid, items):
        for i in self.movies:
         if (i["id"] == mid):
          i["title"] = items[0]
          i["genres"] = items[1]
        self.movies.append({"id" : mid, "title" : items[0], "genres" : items[1]})
		
       def delete_movie(self, mid):
        for i in self.movies:
         if (i["id"] == mid):
          self.movies.remove(i)
 
       def get_user(self, uid):
        ret = list()
        for i in self.users:
         if(i["id"] == uid):
          ret.append(i["gender"])
          ret.append(i["age"])
          ret.append(i["occupation"])
          ret.append(i["zip"])
          return ret
        return None

       def get_users(self):
        ret = list()
        for i in self.users:
         ret.append(i["id"])
        return ret
      	
       def set_user(self, uid, items):
        for i in self.users:
         if(i["id"] == uid):
          i["gender"] = items[0]
          i["age"] = items[1]
          i["occupation"] = items[2]
          i["zip"] = items[3]
        self.users.append({"id" : uid, "gender" : items[0], "age" : items[1], "occupation" : items[2], "zip" : items[3]})

       def delete_user(self, uid):
        for i in self.users:
         if(i["id"] == uid):
          self.users.remove(i)

       def delete_all_ratings(self):
        self.ratings.clear()

       def load_ratings(self, ratings_file):
        self.ratings = dict()
        f = open(ratings_file)
        for line in f:
                line = line.rstrip()
                components = line.split("::")
                uid = int(components[0])
                mid = int(components[1])
                rating = int(components[2])
                ratingDict = {uid : rating}
                if mid not in self.ratings.keys():
                 self.ratings.update({mid : ratingDict})
                else:
                 self.ratings[mid].update(ratingDict)
        f.close()

       def get_rating(self, mid):
        avg = 0; 
        if mid in self.ratings.keys():
         avg = sum(self.ratings[mid].values())/len(self.ratings[mid].values())
        return avg

       def get_highest_rated_movie(self):
        if len(self.ratings) == 0:
         return None
        max_rating = 0
        max_movie = 0
        for i in sorted(self.ratings.keys(), reverse=True):
         if self.get_rating(i) >= max_rating:
          max_rating = self.get_rating(i)
          max_movie = i
        return max_movie
       
       def get_recommendation(self, uid):
        if len(self.ratings) == 0:
         return None
        max_rating = 0
        max_movie = 0
        for i in sorted(self.ratings.keys(), reverse=True):
         if self.get_rating(i) >= max_rating and uid not in self.ratings[i].keys():
          max_rating = self.get_rating(i)
          max_movie = i
        return max_movie
   
       def delete_movies(self):
        self.movies.clear()
 
       def set_user_movie_rating(self, uid, mid, rating):
        if mid in self.ratings.keys():
         self.ratings[mid].update({uid : rating})
        else:
         rating_dict = {uid : rating}
         self.ratings.update({mid : rating_dict})

       def get_user_movie_rating(self, uid, mid):
        if mid in self.ratings.keys():
         if uid in self.ratings[mid].keys():
          return self.ratings[mid][uid] 
        return None

if __name__ == "__main__":
       mdb = _movie_database()

       #### MOVIES ########
       mdb.load_movies('ml-1m/movies.dat')
       mdb.load_ratings('ml-1m/ratings.dat')
       #mdb.print_sorted_movies()
       print(mdb.get_highest_rated_movie())
       print(mdb.get_rating(mdb.get_highest_rated_movie()))
