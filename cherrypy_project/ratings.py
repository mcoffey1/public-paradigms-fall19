import cherrypy
import json
from _movie_database import _movie_database

class RatingController(object):

	def __init__(self, mdb=None):
		if mdb is None:
			self.mdb = _movie_database()
		else:
			self.mdb = mdb

		self.mdb.load_ratings('/home/paradigms/ml-1m/ratings.dat')

	def GET_RATING(self, movie_id):
		output = {'result': 'success'}
		movie_id = int(movie_id)

		try:
			rating = self.mdb.get_rating(movie_id)
			if rating is not None:
				output['movie_id'] = movie_id
				output['rating'] = rating
			else:
				output['result'] = 'error'
				output['message'] = 'movie not found'
		except Exception as ex:
			output['result'] = 'error'
			output['message'] = str(ex)

		return json.dumps(output)
