import re, json
import cherrypy
from _movie_database import _movie_database

class UserController(object):

	def __init__(self, mdb=None):
		if mdb is None:
			self.mdb = _movie_database()
		else:
			self.mdb = mdb

		self.mdb.load_users('/home/paradigms/ml-1m/users.dat')
		#TODO load other resources?

	def GET_ALL(self):
		output = {'result': 'success'}
		output['users'] = list()

		for user in self.mdb.users:
			if user is not None:
				output['users'].append(user)

		return json.dumps(output)
			

	def GET_USER(self, user_id):
		user_id = int(user_id)
		output = {'result': 'success'}

		try:
			user = self.mdb.get_user(user_id)
			if user is not None:
				output['gender'] = user[0]
				output['age'] = user[1]
				output['occupation'] = user[2]
				output['zipcode'] = user[3]
				output['id'] = user_id
			else:
				output['result'] = 'error'
				output['message'] = 'user not found'
		except Exception as ex:
			output['result'] = 'error'
			output['message'] = str(ex)

		return json.dumps(output)

	def PUT_USER(self, user_id):
		output = {'result':'success'}
		user_id = int(user_id)

		data = cherrypy.request.body.read()
		data = json.loads(data)

		items = list()
		items.append(data['gender'])
		items.append(data['age'])
		items.append(data['occupation'])
		items.append(data['zipcode'])

		try:
			self.mdb.set_user(user_id, items)
		except Exception as ex:
			output['result'] = 'error'
			output['message'] = str(ex)

		return json.dumps(output)

	def POST_USER(self):
		output = {'result': 'success'}

		data = cherrypy.request.body.read()
		data = json.loads(data)

		user_id = self.mdb.users[-1]["id"] + 1

		items = list()
		items.append(data['gender'])
		items.append(data['age'])
		items.append(data['occupation'])
		items.append(data['zipcode'])

		try:
			output.update({"id":user_id})
			self.mdb.set_user(user_id, items)
		except Exception as ex:
			output['result'] = 'error'
			output['message'] = str(ex)
		return json.dumps(output)

	def DELETE_USER(self, user_id):
		output = {'result': 'success'}
		user_id = int(user_id)

		try:
			self.mdb.delete_user(user_id)
		except KeyError as ex:
			output['result'] = 'error'
			output['message'] = 'key not found'
		except Exception as ex:
			output['result'] = 'error'
			output['message'] = str(ex)

		return json.dumps(output)

	def DELETE_ALL(self):
		output = {'result': 'success'}
		try:
			self.mdb.users.clear()
		except Exception as ex:
			output['result'] = 'error'
			output['message'] = str(ex)
		# self.mdb.clear()
		return json.dumps(output)



