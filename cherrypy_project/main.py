#incomplete code
import cherrypy

# create movies.py, users.py, etc. in the curr dir
from movies import MovieController
from users import UserController
from votes import VoteController
from ratings import RatingController
from reset import ResetController

# copy your fully working python primer
from _movie_database import _movie_database

def start_service():
    dispatcher = cherrypy.dispatch.RoutesDispatcher()

    # instantiate mdb so that it is shared with all controllers

    mdb_o = _movie_database()

    # instantiate controllers
    movieController = MovieController(mdb=mdb_o)
    userController  = UserController(mdb=mdb_o)
    voteController  = VoteController(mdb=mdb_o)
    ratingController = RatingController(mdb=mdb_o)
    resetController = ResetController(mdb=mdb_o)

    #connecting endpoints

    #connect /movies/:movie_id resource
    dispatcher.connect('movie_get_mid', '/movies/:movie_id', controller=movieController, action='GET_KEY', conditions=dict(method=['GET']))
    dispatcher.connect('movie_put_mid', '/movies/:movie_id', controller=movieController, action='PUT_KEY', conditions=dict(method=['PUT']))
    dispatcher.connect('movie_delete_mid', '/movies/:movie_id', controller=movieController, action='DELETE_KEY', conditions=dict(method=['DELETE']))
    dispatcher.connect('movie_get_all', '/movies/', controller=movieController, action='GET_DICT', conditions=dict(method=['GET']))
    dispatcher.connect('movie_post', '/movies/', controller=movieController, action='POST_DICT', conditions=dict(method=['POST']))
    dispatcher.connect('movie_delete_all', '/movies/', controller=movieController, action='DELETE_DICT', conditions=dict(method=['DELETE']))
    dispatcher.connect('get_rec', '/recommendations/:user_id', controller=voteController, action='GET_REC_KEY', conditions=dict(method=['GET']))
    dispatcher.connect('put_rec', '/recommendations/:user_id', controller=voteController, action='PUT_REC_KEY', conditions=dict(method=['PUT']))
    dispatcher.connect('delete_rec', '/recommendations/', controller=voteController, action='DELETE_RECS', conditions=dict(method=['DELETE']))

    dispatcher.connect('reset_all', '/reset/', controller=resetController, action='PUT_INDEX', conditions=dict(method=['PUT']))
    dispatcher.connect('reset_mid', '/reset/:movie_id', controller=resetController, action='PUT_MID', conditions=dict(method=['PUT']))

    dispatcher.connect('rating_get_r', '/ratings/:movie_id', controller=ratingController, action='GET_RATING', conditions=dict(method=['GET']))

    dispatcher.connect('user_get_uid', '/users/:user_id', controller=userController, action='GET_USER', conditions=dict(method=['GET']))
    dispatcher.connect('user_get_all', '/users/', controller=userController, action='GET_ALL', conditions=dict(method=['GET']))
    dispatcher.connect('user_put_uid', '/users/:user_id', controller=userController, action='PUT_USER', conditions=dict(method=['PUT']))
    dispatcher.connect('user_post', '/users/', controller=userController, action='POST_USER', conditions=dict(method=['POST']))
    dispatcher.connect('user_delete_uid', '/users/:user_id', controller=userController, action='DELETE_USER', conditions=dict(method=['DELETE']))
    dispatcher.connect('user_delete_all', '/users/', controller=userController, action='DELETE_ALL', conditions=dict(method=['DELETE']))

    conf = {
        'global' : {
            'server.socket_host' : 'student04.cse.nd.edu',
            'server.socket_port' : 51023,
        },
        '/' : {
            'request.dispatch' : dispatcher,
        }
    }

    cherrypy.config.update(conf)
    app = cherrypy.tree.mount(None, config=conf)
    cherrypy.quickstart(app)


if __name__ == '__main__':
    start_service()

