import json
import cherrypy
from _movie_database import _movie_database


class VoteController(object):
    def __init__(self, mdb=None):
        if mdb is None:
             self.mdb = _movie_database()
        else:
             self.mdb = mdb
        self.mdb.load_ratings('ml-1m/ratings.dat')


    # event handlers
    def GET_REC_KEY(self, user_id):
        # rule 1: set up some default output
        output = {'result': 'success'}

        # rule 2: check your data and type - key and payload
        key = int(user_id)

        # rule 3: try - except blocks
        try:
            movie = self.mdb.get_recommendation(key)
            output.update({"movie_id" : movie})
        except KeyError as ex:
            output['result'] = 'error'
            output['message'] = 'key not found'
        except Exception as ex:
            output['result'] = 'error'
            output['message'] = str(ex)

        return json.dumps(output)

    def PUT_REC_KEY(self, user_id):
        output = {'result':'success'}
        key = int(user_id)
        #extract msg from body
        data = cherrypy.request.body.read()
        data = json.loads(data)

        try:
            mid = int(data["movie_id"])
            rating = int(data["rating"])
            self.mdb.set_user_movie_rating(key, mid, rating)
        except Exception as ex:
            output['result'] = 'error'
            output['message'] = str(ex)

        return json.dumps(output)

    def DELETE_RECS(self):
        output = {'result':'success'}
        try:
            self.mdb.delete_all_ratings()
        except Exception as ex:
            output['result'] = 'error'
            output['message'] = str(ex)
        return json.dumps(output)


